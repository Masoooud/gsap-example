import React, { useEffect, useRef } from "react";
import "./App.scss";
import { TimelineLite, TweenMax, Power3 } from "gsap";

document.title = "GSAP Sample Animation";

function App() {
  let app = useRef(null);
  let images = useRef(null);
  let content = useRef(null);
  let tl = new TimelineLite({ delay: 0.8 });

  useEffect(() => {
    
    // Images vars
    const seaImage = images.firstElementChild;
    const trainImage = images.lastElementChild;

    // Content vars
    const headlineFirst = content.children[0].children[0];
    const headlineSecond = content.children[1].children[0];
    const contentP = content.children[2];

    // Remove init flash
    TweenMax.to(app, 0, { css: { visibility: "visible" } });

    //Images Animation
    tl.from(seaImage, 1.2, { y: 1280, ease: Power3.easeOut }, "Start")
      .from(
        seaImage.firstElementChild,
        2,
        { scale: 1.6, ease: Power3.easeOut },
        0.2
      )
      .from(trainImage, 1.4, { y: 1280, ease: Power3.easeOut }, 0.2)
      .from(
        trainImage.firstElementChild,
        2,
        { scale: 1.6, ease: Power3.easeOut },
        0.2
      );

    //Content Animation
    tl.staggerFrom(
      [headlineFirst.children, headlineSecond.children],
      1,
      {
        y: 44,
        ease: Power3.easeOut,
        delay: 0.8,
      },
      0.15,
      "Start"
    ).from(contentP, 1, { y: 20, opacity: 0, ease: Power3.easeOut }, 1.4);
  }, [tl]);

  return (
    <div className="hero" ref={(el) => (app = el)}>
      <div className="container">
        <dev className="hero-inner">
          <div className="hero-content">
            <div className="hero-content-inner" ref={(el) => (content = el)}>
              <h1>
                <div className="hero-content-line">
                  <div className="hero-content-line-inner">
                    Masoud Moharrami
                  </div>
                </div>
              </h1>
              <h3>
                <div className="hero-content-line">
                  <div className="hero-content-line-inner">
                    Front-end Developer
                  </div>
                </div>
              </h3>
              <p>
                I'm React and React Native Developer. I have done projects
                contains component using Google Map, Push
                Notification(Firebase), Rest API, etc.
              </p>
            </div>
          </div>
          <div className="hero-images">
            <div className="hero-images-inner" ref={(el) => (images = el)}>
              <div className="hero-image sea">
                <img
                  src={"https://picsum.photos/id/1041/1000/1000"}
                  alt="sea"
                />
              </div>
              <div className="hero-image train">
                <img
                  src={"https://picsum.photos/id/1026/1000/1000"}
                  alt="train"
                />
              </div>
            </div>
          </div>
        </dev>
      </div>
    </div>
  );
}

export default App;
